package com.predict360.plugin.customfield.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"text"
})
public class Result {

@JsonProperty("id")
private String id;
@JsonProperty("text")
private String text;
private String department;
private String legalEntity;

public String getDepartment() {
	return department;
}

public void setDepartment(String department) {
	this.department = department;
}

public String getLegalEntity() {
	return legalEntity;
}

public void setLegalEntity(String legalEntity) {
	this.legalEntity = legalEntity;
}


public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getText() {
return text;
}

public void setText(String text) {
this.text = text;
}

}