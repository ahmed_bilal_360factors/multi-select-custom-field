package com.predict360.plugin.customfield.jira.customfields;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.TextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.predict360.plugin.customfield.vo.Results;

import service.Api;

@Named("MultiSelectCustomField")
public class MultiSelectCustomField extends TextCFType {
    private static final Logger log = LoggerFactory.getLogger(MultiSelectCustomField.class);
    @ComponentImport
    private final CustomFieldValuePersister customFieldValuePersister;
    
    @ComponentImport
    private final GenericConfigManager genericConfigManager;
    
    @ComponentImport
    private final Api api;
    
    @Inject
    public MultiSelectCustomField(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager,Api api) {
    super(customFieldValuePersister, genericConfigManager);
    	this.customFieldValuePersister = customFieldValuePersister;
    	this.genericConfigManager = genericConfigManager;
    	this.api=api;
    }
    
    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue,
                                                     final CustomField field,
                                                     final FieldLayoutItem fieldLayoutItem) {
        final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);

        // This method is also called to get the default value, in
        // which case issue is null so we can't use it to add currencyLocale
        //ApplicationUser loogedInUser = api.getLoggedInUser();
        
        if (issue == null) {
            return map;
        }
        String value = (String)issue.getCustomFieldValue(field);
        try {
			Results results = api.executeGet("http://localhost:8080/treeapi/subjarea/?ids="+value, Results.class);
			map.put("subjareas", results.getResults());
			System.out.println(results.getResults().size());
		} catch (Exception e) {
		}
         FieldConfig fieldConfig = field.getRelevantConfig(issue);
         //add what you need to the map here

        return map;
    }
}