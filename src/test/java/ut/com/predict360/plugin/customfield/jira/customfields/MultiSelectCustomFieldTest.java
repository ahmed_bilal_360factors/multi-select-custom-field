package ut.com.predict360.plugin.customfield.jira.customfields;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.predict360.plugin.customfield.jira.customfields.MultiSelectCustomField;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class MultiSelectCustomFieldTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //MultiSelectCustomField testClass = new MultiSelectCustomField();

        throw new Exception("MultiSelectCustomField has no tests!");

    }

}
