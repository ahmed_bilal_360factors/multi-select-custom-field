package ut.com.predict360.plugin.customfield;

import org.junit.Test;
import com.predict360.plugin.customfield.api.MyPluginComponent;
import com.predict360.plugin.customfield.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}